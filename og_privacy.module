<?php
/**
 * @file
 * Code for the Commons Groups feature.
 */

include_once 'og_privacy.features.inc';

/**
 * Implements hook_og_permission_alter().
 */
function og_privacy_og_permission_alter(&$perms) {
  // We set these values programatically based
  // on the value of field_og_subscribe_settings, so we disable them in the UI.
  $perms['subscribe']['roles'] = array();
  $perms['subscribe']['title'] = t('Contribute to the group');
  $perms['subscribe']['description'] = t('This value is set automatically based on the "Group Privacy Settings" field.');
  $perms['subscribe without approval']['roles'] = array();
  $perms['subscribe without approval']['title'] = t('Contribute to the group without approval');
  $perms['subscribe without approval']['description'] = t('This value is set automatically based on the "Group Privacy Settings" field.');
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function og_privacy_ctools_plugin_directory($module, $plugin) {
  if ($module == 'entityreference') {
    return "plugins/entityreference/$plugin";
  }
}

/**
 * Implements hook_modules_enabled().
 *
 * Make sure the og access fields exist when og_access is enabled.
 */
function og_privacy_modules_enabled($modules) {
  if (in_array('og_access', $modules)) {
    features_revert(array('og_privacy' => array('field_base')));
    features_revert(array('og_privacy' => array('field_instance')));
  }
}

/**
 * Implements hook_help().
 * Used for the 3.2 -> 3.3 migration to warn users who have out-of-date groups
 * to make sure they update the group privacy settings.
 * See https://drupal.org/node/2059857 for more information
 */
function og_privacy_help($path, $arg) {
  if (variable_get('og_privacy_needs_update', FALSE)) {
    $message =  '<p>' . t("OG Privacy has added a new field to control group privacy. Please edit your group(s) and select one of the privacy options. Once all groups are
      set, an administrator can dismiss the update notice.") . '</p>';
    if ($path == 'admin/content/groups/update') {
      return $message;
    }
    elseif ($arg[0] == 'node' && $arg[2] == 'edit') {
      $node = menu_get_object();
      if($node->type == 'group' && empty($node->field_og_subscribe_settings)) {
        return $message;
      }
    }
    if (user_access('edit any group content')) {
      $message = t("Group privacy settings !updated.", array('!updated' => l('need to be updated', 'admin/content/groups/update')));
      drupal_set_message($message, 'warning', FALSE);
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Alter the privacy settings fields.
 */
function og_privacy_form_group_node_form_alter(&$form, &$form_state) {
  // The group privacy settings are not required.
  $form['field_og_subscribe_settings'][LANGUAGE_NONE]['#required'] = FALSE;

  if (module_exists('og_access')) {
    // Display private content checkbox only when "Joining requires approval"
    // is selected.
    $form['field_og_access_default_value']['#states'] = array(
      'visible' => array(
        ':input[name="field_og_subscribe_settings[' . LANGUAGE_NONE . ']"]' => array('value' => 'approval'),
      ),
    );

    $form['#after_build'] = array('og_privacy_form_group_node_after_build');
  }

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'og_privacy') . '/css/og_privacy.css',
  );

  // The group access is set on og_privacy_node_presave().
  $form['group_access'][LANGUAGE_NONE]['#required'] = FALSE;
  $form['group_access']['#access'] = FALSE;
}

/**
 * After build callback for the group node form.
 *
 * Display the private content checkbox inside the privacy settings field.
 */
function og_privacy_form_group_node_after_build($form, $form_state) {
  $form['field_og_subscribe_settings'][LANGUAGE_NONE]['approval']['#suffix'] = render($form['field_og_access_default_value']);

  return $form;
}

/**
 * Update the group permission field.
 *
 * @param $role
 *   The OG role object of which the permissions are being changed.
 * @param $permissions
 *   The anonymous user permissions of the group.
 */
function _og_privacy_update_group_permissions($role, $permissions) {
  $updated_roles = &drupal_static(__FUNCTION__);
  if (!empty($updated_roles[$role->rid])) {
    // Avoid updating a group subscription twice on the same request.
    return;
  }

  if (!empty($permissions['subscribe without approval'])) {
    $subscribe_type = 'anyone';
  }
  elseif (!empty($permissions['subscribe'])) {
    $subscribe_type = 'approval';
  }
  else {
    $subscribe_type = 'invitation';
  }

  $wrapper = entity_metadata_wrapper($role->group_type, $role->gid);
  if ($wrapper->field_og_subscribe_settings->value() != $subscribe_type) {
    // Mark that the group's permissions were already handled on this request,
    // to avoid saving the group entity more than once.
    $updated_roles[$role->rid] = TRUE;

    $wrapper->field_og_subscribe_settings->set($subscribe_type);
    $wrapper->save();
  }
}

/*
 * Implements hook_menu
 * Used with og_privacy_help and the commons groups update view to turn off
 * the warning message to update groups
 */
function og_privacy_menu() {
  $items['admin/content/groups/update/toggle'] = array(
    'title' => 'Toggle Groups Update',
    'page callback' => 'og_privacy_update_toggle',
    'access arguments' => array('edit any group content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/*
 * Ajax callback page to toggle the group update status to off
 * See https://drupal.org/node/2059857 for more information
 */
function og_privacy_update_toggle() {
  variable_set('og_privacy_needs_update', FALSE);
  return TRUE;
}

/* set og_privacy form alter to happen after views bulk operations */
function og_privacy_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_alter') {
    $group = $implementations['og_privacy'];
    unset($implementations['og_privacy']);
    $implementations['og_privacy'] = $group;
  }
}

/**
 * Implements hook_form_alter().
 */
function og_privacy_form_alter(&$form, &$form_state, $form_id) {
  // Redirect the user back to the group homepage after submitting
  // a node within a group.
  if (isset($form['#node']) && substr($form_id, -10) == '_node_form') {
    // Hide the "Group content visibility" field to simplify the node form.
    if (!empty($form['group_content_access']['#access'])) {
      $form['group_content_access']['#access'] = FALSE;
    }
  }
  if (in_array($form_id, array('og_ui_admin_global_permissions', 'og_ui_admin_permissions'))) {
    $group_content_entity_types = og_privacy_get_group_content_entity_types();
    if (!empty($group_content_entity_types)) {
      // @TODO: Improve this message to be more specific and/or
      // reflect these changes in the checkboxes.
      $message = 'In addition to the permissions listed here, the OG Privacy module grants non-group members the ability to post content into groups where content in the group is public.';
      drupal_set_message(t($message), 'warning');
    }
  }
}

/**
 * Implements hook_og_user_access_alter().
 *
 * Deny create permissions from non-members on "non-public" groups (i.e. groups
 * that don't allow joining without approval).
 */
function og_privacy_og_user_access_alter(&$perm, $context) {
  $account = $context['account'];
  $group_type = $context['group_type'];
  $group = $context['group'];

  if ($group_type != 'node') {
    return;
  }
  // The purpose of this function is to grant permissions to create content
  // in a group to non-members of the group, when the group's privacy settings
  // (field_og_subscribe_settings) is set to "Anyone can contribute".
  if (og_is_member($group_type, $group->nid, 'user', $account, array(OG_STATE_ACTIVE, OG_STATE_PENDING, OG_STATE_BLOCKED))) {
    // The user is a group member, so comply to the OG permissions.
    return;
  }

  $wrapper = entity_metadata_wrapper($group_type, $group);
  $access_create = $account->uid && $wrapper->field_og_subscribe_settings->value() == 'anyone';

  // Make sure user can view group (i.e. it's not private).
  $og_privacy_entity_types = og_privacy_get_group_content_entity_types();
  foreach (array_keys($og_privacy_entity_types['node']) as $type) {
    $perm["create $type content"] = $access_create;
  }
}

/**
 * Default value function for the og_group_ref reference field.
 * This function is assigned to the field with the default_value_function
 * property defined in our instances of the og_group_ref field,
 * which takes place in og_privacy_field_definition().
 */
function og_privacy_entityreference_default_value($entity_type, $entity, $field, $instance, $langcode) {

  $items = array();
  $field_name = $field['field_name'];

  if (empty($_GET[$field_name]) || !is_string($_GET[$field_name])) {
    return $items;
  }

  if (empty($instance['settings']['behaviors']['prepopulate']['status'])) {
    return $items;
  }

  $ids = explode(',', $_GET[$field_name]);
  // Check access to the provided entities.
  $target_type = $field['settings']['target_type'];
  entity_load($target_type, $ids);
  // Remove group nodes hidden by the node access system.
  foreach ($ids as $target_id) {
    $target = entity_load_single($target_type, $target_id);
    if (entity_access('view', $target_type, $target)
      && og_is_group_type($target_type, $target->type)
      && (og_user_access($target_type, $target_id, "create $entity->type content") || og_user_access($target_type, $target_id, "update any $entity->type content"))
    ) {
      $items[] = array('target_id' => $target_id);
    }
  }
  return $items;
}

/**
 * Implements hook_node_presave().
 *
 * When the node's group is private, force the group content to be private.
 */
function og_privacy_node_presave($node) {
  if (!module_exists('og_access')) {
    return;
  }

  $wrapper = entity_metadata_wrapper('node', $node);

  if (og_is_group('node', $node)) {
    // Determine whether the group is private according to the subscription
    // field.
    $private = $wrapper->field_og_subscribe_settings->value() == 'invitation';
    $wrapper->{OG_ACCESS_FIELD}->set((int)$private);
    return;
  }

  if (!og_is_group_content_type('node', $node->type)) {
    return;
  }

  // Check whether any of the groups are private.
  $private = FALSE;
  foreach (array_keys(og_get_group_audience_fields('node', $node->type)) as $field) {
    if (empty($node->$field)) {
      continue;
    }

    foreach ($wrapper->$field as $group_wrapper) {
      if (empty($group_wrapper->field_og_access_default_value)) {
        continue;
      }

      if ($group_wrapper->field_og_access_default_value->value() == TRUE) {
        // Once a private group was found, there's no need to continue.
        $private = TRUE;
        break 2;
      }
    }
  }

  if ($private) {
    $wrapper->{OG_CONTENT_ACCESS_FIELD}->set(OG_CONTENT_ACCESS_PRIVATE);
  }
}

/**
 * Implements hook_node_update().
 */
function og_privacy_node_update($node) {
  $account = user_load($node->uid);
  og_privacy_first_contribution($account, $node);

  if (og_is_group('node', $node)) {
    og_privacy_set_group_permissions($node);
  }
}

/**
 * Implements hook_node_insert().
 */
function og_privacy_node_insert($node) {
  $account = user_load($node->uid);
  og_privacy_first_contribution($account, $node);

  if (og_is_group('node', $node)) {
    // When creating a new group, this hook happens before OG creates the
    // group specific roles. Therefore we create the roles here before altering
    // them in og_privacy_set_group_permissions().
    og_roles_override('node', $node->type, $node->nid);
    og_privacy_set_group_permissions($node);
  }
}

/**
 * Set the group's permissions according to field_og_subscribe_settings.
 *
 * @param $node
 *   A group node.
 */
function og_privacy_set_group_permissions($node) {
  // Avoid updating a group subscription twice on the same request.
  $updated_nodes = & drupal_static(__FUNCTION__);
  if (!empty($updated_nodes[$node->nid])) {
    return;
  }
  $updated_nodes[$node->nid] = TRUE;

  $wrapper = entity_metadata_wrapper('node', $node);
  $permission = $wrapper->field_og_subscribe_settings->value();
  $og_roles = og_roles('node', $node->type, $node->nid);
  $anon_rid = array_search(OG_ANONYMOUS_ROLE, $og_roles);

  $permissions = array(
    'subscribe' => $permission == 'approval',
    'subscribe without approval' => $permission == 'anyone',
  );

  // Check if the permissions needs to be changed.
  $changed = FALSE;
  $old_permissions = og_role_permissions(array($anon_rid => OG_ANONYMOUS_ROLE));
  foreach ($permissions as $permission => $value) {
    if (empty($old_permissions[$anon_rid][$permission]) || $old_permissions[$anon_rid][$permission] != $value) {
      $changed = TRUE;
    }
  }

  // Only change the permissions when neccessary.
  if ($changed) {
    og_role_change_permissions($anon_rid, $permissions);
  }
}

/**
 * Returns an array of entity types that are enabled via Commons Groups.
 */
function og_privacy_get_group_content_entity_types() {
  // Find all Commons Entity integrations.
  $commons_entity_integrations = commons_entity_integration_info();
  if (empty($commons_entity_integrations)) {
    return array();
  }

  foreach ($commons_entity_integrations as $entity_type => $integration) {
    foreach ($integration as $bundle => $options) {
      if (isset($options['is_group_content']) && $options['is_group_content'] == FALSE) {
        unset($commons_entity_integrations[$entity_type][$bundle]);
      }
    }
    // If an entity type has no integrations, don't return it.
    if (empty($commons_entity_integrations[$entity_type])) {
      unset($commons_entity_integrations[$entity_type]);
    }
  }

  return $commons_entity_integrations;

}

/**
 * When a user first creates content within a group,
 * grant her the contributor role within that group.
 */
function og_privacy_first_contribution($account, $node) {
  // Find the groups that this piece of content belongs to.
  $groups = og_get_entity_groups('node', $node);
  // @todo: Make it work also with user-groups.
  if (!empty($groups['node'])) {
    $node_groups = array_values($groups['node']);
    // Find the groups that the node author belongs to.
    $account_groups = og_get_groups_by_user($account, 'node');
    if (!$account_groups) {
      $account_groups = array();
    }
    // For groups where this user is not already a member, add her to the group.
    // Anonymous users should never be added to a group automatically
    if ($account->uid == 0) {
      return;
    }
    $new_groups = array_diff($node_groups, $account_groups);
    if (!empty($new_groups)) {
      foreach ($new_groups as $new_group_nid) {
        og_group('node', $new_group_nid, array('entity' => $account->uid));
      }
    }
  }
}

/**
 * Implements hook_field_access().
 */
function og_privacy_field_access($op, $field, $entity_type, $entity, $account) {
  $field_name = $field['field_name'];
  switch ($field_name) {
    case 'og_roles_permissions':
      return FALSE;

    case 'field_og_access_default_value':
      return $op == 'edit' && module_exists('og_access');

    case 'field_og_subscribe_settings':
      return $op == 'edit';
  }

  if (module_exists('og_access') && in_array($field_name, array(OG_CONTENT_ACCESS_FIELD, OG_ACCESS_FIELD))) {
    return FALSE;
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function og_privacy_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  global $user;
  $account = clone $user;

  if (!og_is_group($entity_type, $entity)) {
    return;
  }

  list($id, , $bundle) = entity_extract_ids($entity_type, $entity);

  // The user has a pending membership request. Let her know that
  // her request is pending review.
  if (og_is_member($entity_type, $id, 'user', $account, array(OG_STATE_PENDING))) {
    $element[0] = array('#markup' => '<div class="subscription-type">' . t('Your membership request is pending review by a group organizer.') . '</div>');
    return $element;
  }
  // If user is blocked, they should not be able to apply for membership.
  if (og_is_member($entity_type, $id, 'user', $account, array(OG_STATE_BLOCKED))) {
    return;
  }
  if (og_is_member($entity_type, $id, 'user', $account)) {
    // The user has an active membership. She can leave the group.
    // For groups where anyone can contribute without joining, don't display
    // a "Leave" link since users never went through
    // the separate step of joining.
    if (og_is_member($entity_type, $id, 'user', $account, array(OG_STATE_ACTIVE)) && $entity->field_og_subscribe_settings[LANGUAGE_NONE][0]['value'] != 'anyone') {
      $links['title'] = t('Leave group');
      $links['href'] = "group/$entity_type/$id/unsubscribe";
    }
  }
  else {
    // Check if user can subscribe to the field.
    if (empty($settings['field_name']) && $audience_field_name = og_get_best_group_audience_field('user', $account, $entity_type, $bundle)) {
      $settings['field_name'] = $audience_field_name;
    }
    if (!$settings['field_name']) {
      return;
    }

    $field_info = field_info_field($settings['field_name']);

    // Check if entity is referencable.
    if ($field_info['settings']['target_type'] != $entity_type) {
      // Group type doesn't match.
      return;
    }
    if (!empty($field_info['settings']['handler_settings']['target_bundles']) && !in_array($bundle, $field_info['settings']['handler_settings']['target_bundles'])) {
      // Bundles don't match.
      return;
    }

    if (!og_check_field_cardinality('user', $account, $settings['field_name'])) {
      $element[0] = array('#markup' => format_plural($field_info['cardinality'], 'You are already registered to another group', 'You are already registered to @count groups'));
      return $element;
    }

    $url = "group/$entity_type/$id/subscribe";
    if ($settings['field_name']) {
      $url .= '/' . $settings['field_name'];
    }
    // Set the needs update hook if we end up with a group call that is missing
    // the subscribe settings. We also check the variable first, because we
    // don't want to reset the variable cache if we don't have to.
    // See https://drupal.org/node/2059857#comment-7733465 for more info.
    if (empty($entity->field_og_subscribe_settings)) {
      if (!variable_get('og_privacy_needs_update', FALSE)) {
        variable_set('og_privacy_needs_update', TRUE);
      }
    }
    // Don't display join link on public groups.
    else {
      if ($entity->field_og_subscribe_settings[LANGUAGE_NONE][0]['value'] != 'anyone') {
        if ($entity->field_og_subscribe_settings[LANGUAGE_NONE][0]['value'] == 'approval') {
          $subscription_type = t('Moderated group');
          $links['title'] = t('Join group');
          if ($account->uid) {
            $links['href'] = $url;
          }
          else {
            $links['href'] = 'user/login';
            $links['options'] = array('query' => array('destination' => $url));
          }
        }
        else {
          $element[0] = array('#markup' => '<div class="subscription-type">' . t('Invite-only group') . '</div>');
          return $element;
        }
      }
    }
  }
  if (!empty($links['title'])) {
    $links += array('options' => array());
    $element[0] = array(
      '#type' => 'link',
      '#title' => $links['title'],
      '#href' => $links['href'],
      '#options' => $links['options'],
    );

    if (!empty($subscription_type)) {
      $element[0]['#prefix'] = '<div class="subscription-type">' . $subscription_type . '</div>';
    }

    return $element;
  }
}
